import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers";

export default function configureStore(initialState) {
  // const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  const store = createStore(reducers);

  if (module.hot) {
    // console.log(reducers);
    module.hot.accept(() => {
      const nextRootReducer = require("./reducers/index").default;
      store.replaceReducer(nextRootReducer);
    });
  }
  return store;
}
