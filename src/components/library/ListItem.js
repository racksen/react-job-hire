import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, LayoutAnimation} from "react-native";
import {connect} from 'react-redux';
import { CardSection } from "../common";
import * as actions from '../../redux/actions';

class ListItem extends Component {
  
  componentWillUpdate(nextProps, nextState) {
    LayoutAnimation.spring();
  }
  

  renderDescription() {
    const { description } = this.props.library;
    
    if(this.props.expanded){
      return (
        <CardSection>
          <Text style={{flex:1}}>{description}</Text>
        </CardSection>
      );
    }
  }
  
  render() {
    const { id, title } = this.props.library;
    const { titleStyle } = styles;
  
    return (
      <TouchableWithoutFeedback onPress ={ ()=>this.props.selectLibrary(id)}>
        <View>
          <CardSection>
            <Text style={titleStyle}>{title}</Text>
          </CardSection>
          {this.renderDescription()}
        </View>
      </TouchableWithoutFeedback>
    );  
  }
}

const styles = {
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15
  }
}

const mapStateToProps = (state, ownProps) => {
  const expanded = state.selectedLibraryID === ownProps.library.id;
  return {
    expanded
  };
};

export default connect(mapStateToProps, actions)(ListItem);