import React from "react";
import { StyleSheet } from "react-native";
import firebase from 'firebase'
import { Card, CardSection, Button, Input, Spinner } from "../common";

const Logout = ({onPress}) => {
  return (
    <Card>
      <CardSection>
      <Button onPress={() => firebase.auth().signOut()}>Logout</Button>
      </CardSection>
    </Card>
  );
};


const styles = StyleSheet.create({
});

export { Logout };
