import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

class Header extends Component {

  render() {
    const { containerStyle, textStyle } = styles;
    return (
      <View style={containerStyle}>
        <Text style={textStyle}>{this.props.title}</Text>
      </View>
    );
  }
}


const styles = StyleSheet.create({
   containerStyle: {
    justifyContent: 'center',
    backgroundColor: '#F8F8F8',
    alignItems: 'center',
    paddingTop: 15,
    height: 100,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 5},
    shadowOpacity: 0.8,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    color: '#000',
    fontSize: 30
  }
});

export { Header };
